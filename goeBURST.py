#!/usr/bin/env python3
import os
import sys
import argparse
from modules.functions import *

def parse_arguments():
    description = """
    Run goeBURST on ST profiles. Format
    ST	arcC	aroE	glpF	gmk	pta	tpi	yqiL
    1	1	1	1	1	1	1	1
    5	1	4	1	4	12	1	10
    7	5	4	1	4	4	6	3
    8	3	3	1	1	4	4	3
    """
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter,)
    parser.add_argument('-i', '--st_profiles_file', help='path to ST profiles file', required = True, type = str)
    parser.add_argument('-o', '--output_file_prefix', help='path to output file prefix', default = 'goeBURST', type = str)
    parser.add_argument('-c', '--cutoff', help='cutoff when linking profiles, e.g 1 (default) SLVs, 2 to allow DLVs etc', default = 1, type = int)
    parser.add_argument('-p', '--output_png_files', help='output image file of complete and cutoff-based graphs', action='store_true')
    parser.add_argument('-w', '--png_width', help='image width (inches) - default 10', default = 10, type = int)
    parser.add_argument('-l', '--png_length', help='image length (inches) - default 10', default = 10, type = int)
    parser.add_argument('-d', '--output_dot_file', help='output dot file', action='store_true')
    parser.add_argument('-t', '--output_txt_file', help='output TSV file which records goeEBURST groups', action='store_true')
    return parser.parse_args()


def main(options):
    labelled_tree_with_distances = run_goeBURST(options.st_profiles_file)
    complete_graph, split_graph = make_graph(labelled_tree_with_distances, options.cutoff)

    if (options.output_png_files):
        output_png_files(complete_graph, split_graph, options.output_file_prefix, options.cutoff, options.png_width, options.png_length)
    if (options.output_dot_file):
        output_dot_file(complete_graph, options.output_file_prefix)
    if (options.output_txt_file):
        output_text_file(split_graph, options.output_file_prefix, options.cutoff)


if __name__ == "__main__":
    options = parse_arguments()
    main(options)
