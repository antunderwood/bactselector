# goeEBURST implementation
Based on the [code](https://github.com/jacarrico/goeBURST) from [João André Carriço](https://github.com/jacarrico)

## Example of how to run
 * Produce a png image file of the goeEBURST complete MST and groups based on SLVs (image width and height 20 inches)
 * Produce a dot file
 * Produce a TSV file with the columns
   * goeEBURST group founder
   * Well-connected goeEBURST group members
   * Number of STs
   * goeEBURST group members
```
python3 goeBURST.py -i data/sau_500_profiles.tsv  -o test_outputs/sau_500_goeBURST -c 1 -p -w 20 -l 20 -d -t
```
 
 * Produce just a TSV file with goeEBURST groups that allow DLVs

```
python3 goeBURST.py -i data/sau_500_profiles.tsv  -o test_outputs/sau_500_goeBURST -c 2 -t
```


## Usage
```
usage: goeBURST.py [-h] -i ST_PROFILES_FILE [-o OUTPUT_FILE_PREFIX]
                   [-c CUTOFF] [-p] [-w PNG_WIDTH] [-l PNG_LENGTH] [-d] [-t]

    Run goeBURST on ST profiles. Format
    ST	arcC	aroE	glpF	gmk	pta	tpi	yqiL
    1	1	1	1	1	1	1	1
    5	1	4	1	4	12	1	10
    7	5	4	1	4	4	6	3
    8	3	3	1	1	4	4	3


optional arguments:
  -h, --help            show this help message and exit
  -i ST_PROFILES_FILE, --st_profiles_file ST_PROFILES_FILE
                        path to ST profiles file
  -o OUTPUT_FILE_PREFIX, --output_file_prefix OUTPUT_FILE_PREFIX
                        path to output file prefix
  -c CUTOFF, --cutoff CUTOFF
                        cutoff when linking profiles, e.g 1 (default) SLVs, 2
                        to allow DLVs etc
  -p, --output_png_files
                        output image file of complete and cutoff-based graphs
  -w PNG_WIDTH, --png_width PNG_WIDTH
                        image width (inches) - default 10
  -l PNG_LENGTH, --png_length PNG_LENGTH
                        image length (inches) - default 10
  -d, --output_dot_file
                        output dot file
  -t, --output_txt_file
                        output TSV file which records goeEBURST groups
```